<?php

namespace ITPolice\Helpers\Cli;

class Message
{
    const M_PROCESS = 'Process';
    const M_INFO = 'Info';
    const M_ERROR = 'Error';
    const M_WARNING = 'Warnig';
    const M_SUCCESS = 'Success';

    private static $foreground_colors = array(
        'black' => '0;30',
        'dark_gray' => '1;30',
        'blue' => '0;34',
        'light_blue' => '1;34',
        'green' => '0;32',
        'light_green' => '1;32',
        'cyan' => '0;36',
        'light_cyan' => '1;36',
        'red' => '0;31',
        'light_red' => '1;31',
        'purple' => '0;35',
        'light_purple' => '1;35',
        'brown' => '0;33',
        'yellow' => '1;33',
        'light_gray' => '0;37',
        'white' => '1;37'
    );

    private static $background_colors = array(
        'black' => '40',
        'red' => '41',
        'green' => '42',
        'yellow' => '43',
        'blue' => '44',
        'magenta' => '45',
        'cyan' => '46',
        'light_gray' => '47'
    );

    public static function show($message = false, $type = false, $exit = NULL)
    {

        if (!$message) {
            $message = "No message";
            $type = self::M_ERROR;
        }
        if (!$type) {
            $type = self::M_PROCESS;
        }

        if ($type == self::M_ERROR) {
            $message = "$type: $message\n";
            if ($exit === NULL)
                $exit = true;
        }
        switch ($type) {
            case self::M_ERROR:
                $cli_foreground_color = 'red';
                break;
            case self::M_INFO:
                $cli_foreground_color = 'cyan';
                break;
            case self::M_SUCCESS:
                $cli_foreground_color = 'green';
                break;
            case self::M_WARNING:
                $cli_foreground_color = 'yellow';
                break;
            case self::M_PROCESS:
                $cli_foreground_color = null;
                break;
            default:
                $cli_foreground_color = null;
                break;
        }
        echo self::getColoredString($message, $cli_foreground_color);
        echo "\n";

        if ($type == self::M_ERROR && $exit) {
            debug_backtrace();
        }

//        if ($report_email && ($type == self::M_ERROR || $type == self::M_SUCCESS) && $exit) {
//            mail(self::$report_email, 'Parsing report: ' . self::$parser_name, $message);
//        }
        if ($exit) {
            exit;
        }
    }

    // Returns colored string
    private static function getColoredString($string, $foreground_color = null, $background_color = null)
    {
        $colored_string = "";

        // Check if given foreground color found
        if (isset(self::$foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . self::$foreground_colors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset(self::$background_colors[$background_color])) {
            $colored_string .= "\033[" . self::$background_colors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .= $string . "\033[0m";

        return $colored_string;
    }

    // Returns all foreground color names
    private function getForegroundColors()
    {
        return array_keys(self::$foreground_colors);
    }

    // Returns all background color names
    private function getBackgroundColors()
    {
        return array_keys(self::$background_colors);
    }
}