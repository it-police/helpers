<?php

namespace ITPolice\Helpers\Cli;

class Progress
{

    public $total = 0;
    public $done = 0;

    public function __construct($total = false)
    {
        if ($total != false) {
            $this->total = intval($total);
        }

        return $this;
    }

    public function inc()
    {
        $this->done++;
        return $this;
    }

    public function show($done = false)
    {
        if($this->total) {
            if ($done != false) {
                $this->done = intval($done);
            }
            $done = $this->done;
            $total = $this->total;

            $perc = floor(($this->done / $this->total) * 100);
            $left = 100 - $perc;
            $write = sprintf("\033[0G\033[2K[%'={$perc}s>%-{$left}s] - $perc%% - $done/$total", "", "");
            fwrite(STDERR, $write);

            if ($this->done == $this->total) {
                echo "\n";
            }
        }

        return $this;
    }

    function done()
    {
        echo ($this->total ? "" : "total zero")."\n";
    }
}