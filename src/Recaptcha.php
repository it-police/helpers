<?
namespace ITPolice\Helpers;

/*
reCaptcha представляет собой бесплатный сервис, который защищает ваш сайт от спама и злоупотреблений.

// Подключение
<?=ReCaptcha::getHtml();?>

// Проверка
if(!ReCaptcha::check()) {
    throw new \Exception(ReCaptcha::$msg);
    return false;
}
*/
class ReCaptcha
{
	static public $publicKey = '';
	static public $secretKey = '';
	static public $msg;

	static function addJs() {
        return "<script type='text/javascript' src='https://www.google.com/recaptcha/api.js'></script>";
	}

	static function getHtml() {
		self::addJs();
		return '<div class="g-recaptcha" data-sitekey="'.self::$publicKey.'"></div>';
	}

	static function check() {
		self::$msg = '';
		if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$recaptcha=$_POST['g-recaptcha-response'];

			if(!empty($recaptcha))
			{
				$google_url = "https://www.google.com/recaptcha/api/siteverify";
				$secret = self::$secretKey;
				$ip = $_SERVER['REMOTE_ADDR'];

				$data = array(
		            'secret' => $secret,
		            'response' => $recaptcha,
		            'remoteip' => $ip
		        );

				$verify = curl_init();
				curl_setopt($verify, CURLOPT_URL, $google_url);
				curl_setopt($verify, CURLOPT_POST, true);
				curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
				curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
				$res = curl_exec($verify);
				$res = json_decode($res, true);

				//reCaptcha success check 
				if($res['success'])
				{
					return true;
				}
				else
				{
					self::$msg="Ошибка! Проверка не пройдена.";
				}

			}
			else
			{
				self::$msg="Пожалуйста, укажите что вы не робот.";
			}

		}
		return false;
	}
}


