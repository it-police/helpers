<?
namespace ITPolice\Helpers;

class Functions {

    // склонение по падежам (words = array('рубль','рубля','рублей'))
    public static function wordCase($v,$words){
        $n = intval(substr($v,-2));
        if ($n > 19) { $n %= 10; }
        switch ($n) {
            case 1:
                $text = $words[0];break;
            case 2:
            case 3:
            case 4:
                $text = $words[1];break;

            default:
                $text = $words[2];
        }
        return $text;
    }

    public static function isLocalhost($whitelist = ['127.0.0.1', '::1']) {
        return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
    }
}